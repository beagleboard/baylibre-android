# Documentation:

BayLibre's development tree documentation can be found at:
- https://baylibre.pages.baylibre.com/ti/android/doc/beagleplay.html

For stable releases, refer to the TI SDK found at:
- https://www.ti.com/tool/PROCESSOR-SDK-AM62X

For example, for SDK 9.2 release:
- https://software-dl.ti.com/processor-sdk-android/esd/AM62X/09_02_00/docs/devices/AM62X/android/Application_Notes_BeaglePlay.html

# Prebuilt image
TI provides pre-built Android images for the Beagle Play here:
- https://dr-download.ti.com/software-development/software-development-kit-sdk/MD-8cVNlmT7Aa/09.02.00/AM62x_userdebug_09.02.00_emmc.tar.gz

This image is compatible with TI's development boards and the Beagle Play.

# Docker Image:

```
git clone https://github.com/alsutton/aosp-build-docker-images.git
cd ./aosp-build-docker-images
docker build -f ubuntu-20_04-aosp.dockerfile -t robertcnelson/ubuntu-20_04-aosp .
docker push robertcnelson/ubuntu-20_04-aosp:latest
```

aosp-build-docker-images Changes:

```diff
diff --git a/ubuntu-20_04-aosp.dockerfile b/ubuntu-20_04-aosp.dockerfile
index 72f150f..70f0718 100644
--- a/ubuntu-20_04-aosp.dockerfile
+++ b/ubuntu-20_04-aosp.dockerfile
@@ -24,11 +24,13 @@ RUN DEBIAN_FRONTEND="noninteractive" apt-get install -y git-core gnupg flex biso
 RUN mkdir ~/.gnupg && chmod 600 ~/.gnupg && echo "disable-ipv6" >> ~/.gnupg/dirmngr.conf
 
 # Download and verify repo
-RUN gpg --keyserver pool.sks-keyservers.net --recv-key 8BB9AD793E8E6153AF0F9A4416530D5E920F5C65
+RUN gpg --keyserver pgp.mit.edu --recv-key 8BB9AD793E8E6153AF0F9A4416530D5E920F5C65
 RUN curl -o /usr/local/bin/repo https://storage.googleapis.com/git-repo-downloads/repo
 RUN curl https://storage.googleapis.com/git-repo-downloads/repo.asc | gpg --verify - /usr/local/bin/repo
 RUN chmod a+x /usr/local/bin/repo
 
+RUN DEBIAN_FRONTEND="noninteractive" apt-get install -y ccache python-is-python3 python3
+
 # Create the home directory for the build user
 RUN groupadd -g $groupid $username \
  && useradd -m -s /bin/bash -u $userid -g $groupid $username
```
